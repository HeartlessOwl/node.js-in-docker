const express = require('express');
var morgan = require('morgan')
const fs = require('fs');
var path = require('path')
const app = express();

var visits = 0;

//setting up an access.log
var acesslogger = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})
app.use(morgan(':method :url :status :res[content-length] - :response-time ms :date[web]', {stream: acesslogger}))


//setting up variables with default values
const IP = process.env.IP  || '0.0.0.0';
const PORT = process.env.PORT || 3000;

//server app
app.get('/',(require, response) => {
    console.log('New Visitor');
    visits++;
    fs.writeFileSync('visits.txt', visits.toString());
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.write('Hi!\n');
    response.write('You are ' + visits + ' visitor');
    response.end();

})
app.listen(PORT,IP,() =>{
    console.log(IP,PORT);

console.log('Server is Running..');});
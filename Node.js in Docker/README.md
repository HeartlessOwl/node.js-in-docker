# Node.Js in Docker

This is a Test Task

## Description

This Repository includes all the necessairy files to run a docker container with node.js server. 
The server includes a visit counter, whic is displayed when visiting the website. The value is also stored in visits.txt
Morgan module is used as an access logging tool for this server. 
All the log information could be retrieved from access.log
env.list is responsible for setting the variables in container, this is one of the options of passing variables to Docker with run command. (IP, PORT) These values are printed in console after container starts. 


## Instruction

1. Clone the Repository to your machine
2. Run **docker build -t imagename .**
3. Run **docker run -p 8080:8080 --env-file env.list imagename** (Substitute the ports according to requirements)
4. Done
